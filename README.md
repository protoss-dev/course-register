
## Build Docker Image
sudo docker build -t protosstechnology/ptg-course-register .
## Run Docker Image
sudo docker run -itd -p 4200:80 protosstechnology/ptg-course-register

## Push to Docker Hub
sudo docker tag protosstechnology/ptg-course-register protosstechnology/ptg-course-register
sudo docker push protosstechnology/ptg-course-register
