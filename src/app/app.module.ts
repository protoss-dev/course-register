
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http'

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { VueComponent } from './vue/vue.component';
import { MongoDbComponent } from './mongo-db/mongo-db.component';
import { DetailsvueComponent } from './vue/detailsvue/detailsvue.component';
import { CovervueComponent } from './vue/covervue/covervue.component';
import { RegistervueComponent } from './vue/registervue/registervue.component';

import { GolangComponent } from './golang/golang.component';
import { DetailgolangComponent } from './golang/detailgolang/detailgolang.component';
import { RegistergolangComponent } from './golang/registergolang/registergolang.component';
import { CovergoComponent } from './golang/covergo/covergo.component';
import { KotlinComponent } from './kotlin/kotlin.component';
import { CoverkotlinComponent } from './kotlin/coverkotlin/coverkotlin.component';
import { DetailkotlinComponent } from './kotlin/detailkotlin/detailkotlin.component';
import { RegisterkotlinComponent } from './kotlin/registerkotlin/registerkotlin.component';


import { DetailmongoDbComponent } from './mongo-db/detailmongo-db/detailmongo-db.component';
import { RegistermongoDbComponent } from './mongo-db/registermongo-db/registermongo-db.component';
import { CovermongoDbComponent } from './mongo-db/covermongo-db/covermongo-db.component';

import { LineDeveloperComponent } from './line-developer/line-developer.component';

import { CoverlineDeveloperComponent } from './line-developer/coverline-developer/coverline-developer.component';
import { DetaillineDeveloperComponent } from './line-developer/detailline-developer/detailline-developer.component';
import { RegisterllineDeveloperComponent } from './line-developer/registerlline-developer/registerlline-developer.component';

import { HtmlCssComponent } from './html-css/html-css.component';
import { CoverhtmlCssComponent } from './html-css/coverhtml-css/coverhtml-css.component';
import { DetailhtmlCssComponent } from './html-css/detailhtml-css/detailhtml-css.component';
import { RegisterhtmlCssComponent } from './html-css/registerhtml-css/registerhtml-css.component';
import { GraphQlComponent } from './graph-ql/graph-ql.component';
import { CovergraphqlComponent } from './graph-ql/covergraphql/covergraphql.component';
import { DetailgraphqlComponent } from './graph-ql/detailgraphql/detailgraphql.component';
import { RegistergraphqlComponent } from './graph-ql/registergraphql/registergraphql.component';
import { AndroidComponent } from './android/android.component';
import { CNetComponent } from './c-net/c-net.component';
import { CoverAndroidComponent } from './android/cover-android/cover-android.component';
import { CovercNetComponent } from './c-net/coverc-net/coverc-net.component';
import { DetailsCnetComponent } from './c-net/details-cnet/details-cnet.component';
import { RegisterCnetComponent } from './c-net/register-cnet/register-cnet.component';
import { DetailsAndroidComponent } from './android/details-android/details-android.component';
import { RegisterAndroidComponent } from './android/register-android/register-android.component';
import { SqlComponent } from './sql/sql.component';
import { CoverSqlComponent } from './sql/cover-sql/cover-sql.component';
import { RegisterSqlComponent } from './sql/register-sql/register-sql.component';
import { DetailsSqlComponent } from './sql/details-sql/details-sql.component';
import { PythonComponent } from './python/python.component';
import { CoverpythonComponent } from './python/coverpython/coverpython.component';
import { DetailspythonComponent } from './python/detailspython/detailspython.component';
import { RegisterpythonComponent } from './python/registerpython/registerpython.component';
import { JavaComponent } from './java/java.component';
import { CoverjavaComponent } from './java/coverjava/coverjava.component';
import { DetailsjavaComponent } from './java/detailsjava/detailsjava.component';
import { RegisterjavaComponent } from './java/registerjava/registerjava.component';
import { DockerComponent } from './docker/docker.component';
import { CoverdockerComponent } from './docker/coverdocker/coverdocker.component';
import { DetailsdockerComponent } from './docker/detailsdocker/detailsdocker.component';
import { RegisterdockerComponent } from './docker/registerdocker/registerdocker.component';
import { AngularComponent } from './angular/angular.component';
import { CoverangularComponent } from './angular/coverangular/coverangular.component';
import { DetailsangularComponent } from './angular/detailsangular/detailsangular.component';
import { RegisterangularComponent } from './angular/registerangular/registerangular.component';
import { SwiftComponent } from './swift/swift.component';
import { RegisterswiftComponent } from './swift/registerswift/registerswift.component';
import { DetailsswiftComponent } from './swift/detailsswift/detailsswift.component';
import { CoverswiftComponent } from './swift/coverswift/coverswift.component';
import { JavaScriptComponent } from './java-script/java-script.component';
import { CoverjavascriptComponent } from './java-script/coverjavascript/coverjavascript.component';
import { RegisterjavaScriptComponent } from './java-script/registerjava-script/registerjava-script.component';
import { DetailsjavaScriptComponent } from './java-script/detailsjava-script/detailsjava-script.component';
import { NodeJsComponent } from './node-js/node-js.component';
import { RegisternodeJsComponent } from './node-js/registernode-js/registernode-js.component';
import { DetailsnodeJsComponent } from './node-js/detailsnode-js/detailsnode-js.component';
import { CovernodeJsComponent } from './node-js/covernode-js/covernode-js.component';
import { NestJsComponent } from './nest-js/nest-js.component';
import { CovernestJsComponent } from './nest-js/covernest-js/covernest-js.component';
import { DetailsnestJsComponent } from './nest-js/detailsnest-js/detailsnest-js.component';
import { RegisternestJsComponent } from './nest-js/registernest-js/registernest-js.component';



@NgModule({
  declarations: [
    AppComponent,
    VueComponent,
    MongoDbComponent,
    DetailsvueComponent,
    CovervueComponent,
    RegistervueComponent,

    GolangComponent,

    DetailgolangComponent,
    RegistergolangComponent,
    CovergoComponent,
    KotlinComponent,
    CoverkotlinComponent,
    DetailkotlinComponent,
    RegisterkotlinComponent,

    DetailmongoDbComponent,
    RegistermongoDbComponent,
    CovermongoDbComponent,


    LineDeveloperComponent,

    CoverlineDeveloperComponent,
    DetaillineDeveloperComponent,
    RegisterllineDeveloperComponent,

    HtmlCssComponent,
    CoverhtmlCssComponent,
    DetailhtmlCssComponent,
    RegisterhtmlCssComponent,
    GraphQlComponent,

    CovergraphqlComponent,

    DetailgraphqlComponent,

    RegistergraphqlComponent,
    AndroidComponent,
    CNetComponent,
    CoverAndroidComponent,
    CovercNetComponent,
    DetailsCnetComponent,
    RegisterCnetComponent,
    DetailsAndroidComponent,
    RegisterAndroidComponent,
    SqlComponent,
    CoverSqlComponent,
    RegisterSqlComponent,
    DetailsSqlComponent,
    PythonComponent,
    CoverpythonComponent,
    DetailspythonComponent,
    RegisterpythonComponent,
    JavaComponent,
    CoverjavaComponent,
    DetailsjavaComponent,
    RegisterjavaComponent,
    DockerComponent,
    CoverdockerComponent,
    DetailsdockerComponent,
    RegisterdockerComponent,
    AngularComponent,
    CoverangularComponent,
    DetailsangularComponent,
    RegisterangularComponent,
    SwiftComponent,
    RegisterswiftComponent,
    DetailsswiftComponent,
    CoverswiftComponent,
    JavaScriptComponent,
    CoverjavascriptComponent,
    RegisterjavaScriptComponent,
    DetailsjavaScriptComponent,
    NodeJsComponent,
    RegisternodeJsComponent,
    DetailsnodeJsComponent,
    CovernodeJsComponent,
    NestJsComponent,
    CovernestJsComponent,
    DetailsnestJsComponent,
    RegisternestJsComponent,


  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],

  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SweetAlert2Module.forRoot(),
    SweetAlert2Module,
    SweetAlert2Module.forChild({ /* options */ })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
