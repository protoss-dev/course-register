import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterkotlinComponent } from './registerkotlin.component';

describe('RegisterkotlinComponent', () => {
  let component: RegisterkotlinComponent;
  let fixture: ComponentFixture<RegisterkotlinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterkotlinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterkotlinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
