import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CovergoComponent } from './covergo.component';

describe('CovergoComponent', () => {
  let component: CovergoComponent;
  let fixture: ComponentFixture<CovergoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovergoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CovergoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
