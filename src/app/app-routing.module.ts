import { LineDeveloperComponent } from './line-developer/line-developer.component';
import { GolangComponent } from './golang/golang.component';
import { AppComponent } from './app.component';
import { MongoDbComponent } from './mongo-db/mongo-db.component';
import { VueComponent } from './vue/vue.component';
import { SqlComponent } from './sql/sql.component';
import { CNetComponent } from './c-net/c-net.component';
import { AndroidComponent } from './android/android.component';
import { NestJsComponent } from './nest-js/nest-js.component';
import { NodeJsComponent } from './node-js/node-js.component';
import { JavaScriptComponent } from './java-script/java-script.component';
import { SwiftComponent } from './swift/swift.component';
import { AngularComponent } from './angular/angular.component';
import { DockerComponent } from './docker/docker.component';
import { JavaComponent } from './java/java.component';
import { PythonComponent } from './python/python.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KotlinComponent } from './kotlin/kotlin.component';

import { HtmlCssComponent } from './html-css/html-css.component';
import { GraphQlComponent } from './graph-ql/graph-ql.component';

const routes: Routes = [
  {
    path: '',
    component: AppComponent
  },
  {
    path: 'vue',
    component: VueComponent
  },
  {
    path: 'mongo-db',
    component: MongoDbComponent
  },
  {
    path: 'golang',
    component: GolangComponent
  },
  {
    path: 'kotlin',
    component: KotlinComponent
  },
  {
    path: 'graph-ql',
    component: GraphQlComponent
  },
  {
    path: 'line-developer',
    component: LineDeveloperComponent
  },
  {
    path: 'html-css',
    component: HtmlCssComponent
  },
  {
    path: 'android',
    component: AndroidComponent
  },
  {
    path: 'c-net',
    component: CNetComponent
  },
  {
    path: 'sql',
    component: SqlComponent
  },
  {
    path : 'java',
    component : JavaComponent
  },
  {
    path : 'python',
    component : PythonComponent
  },
  {
    path : 'docker',
    component : DockerComponent
  },
  {
    path : 'angular',
    component : AngularComponent
  },
  {
    path : 'swift',
    component : SwiftComponent
  },
  {
    path : 'java-script',
    component : JavaScriptComponent
  },
  {
    path : 'node-js',
    component : NodeJsComponent
  },
  {
    path : 'nest-js',
    component : NestJsComponent
  },
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
