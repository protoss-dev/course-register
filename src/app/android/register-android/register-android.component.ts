import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import Swal from "sweetalert2";

@Component({
  selector: "app-register-android",
  templateUrl: "./register-android.component.html",
  styleUrls: ["./register-android.component.css"]
})
export class RegisterAndroidComponent implements OnInit {
  registerForm: FormGroup;
  submitted = true;

  public results: string[];

  constructor(
    private formBuilder: FormBuilder,
    private httpClient: HttpClient
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: [
        "suriya_e@protossgroup.com",
        [Validators.required, Validators.email]
      ],
      name: ["Suriya Eiamerb", Validators.required],
      phone: [
        "0655941964",
        [Validators.required, Validators.pattern(/^\d{10}$/)]
      ],
      address: ["Android", Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    // display form values on success
    Swal.fire("ลงทะเบียนสำเร็จ!", "คุณได้ทำการลงทะเบียนเรียนแล้ว!", "success");
    // alert(
    //   "SUCCESS!! :-)\n\n" + JSON.stringify(this.registerForm.value, null, 4)
    // );

    let url =
      "https://script.google.com/macros/s/AKfycbzFx4rDH-py3F-vmPQErbllXTN4wWQZq73tZ2IcrDMlI2bR4NE/exec?path=/register";
    let json = {
      email: this.registerForm.value.email,
      fullName: this.registerForm.value.name,
      telephoneNumber: this.registerForm.value.phone,
      course: this.registerForm.value.address
    };
    // let json = this.registerForm.value;

    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded"
      })
    };

    this.httpClient
      .post(url, json, httpOptions)
      .toPromise()
      .then(data => {
        console.log("response = " + data);
      });

    this.httpClient
      .get(
        "https://script.google.com/macros/s/AKfycbzFx4rDH-py3F-vmPQErbllXTN4wWQZq73tZ2IcrDMlI2bR4NE/exec?path=/register"
      )
      .subscribe(response => {
        this.registerForm = response["registerForm"];
      });
  }
}
