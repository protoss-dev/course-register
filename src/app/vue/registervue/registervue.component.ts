import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import Swal from "sweetalert2";

@Component({
  selector: "app-registervue",
  templateUrl: "./registervue.component.html",
  styleUrls: ["./registervue.component.css"]
})
export class RegistervueComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  public results: string[];

  constructor(
    private formBuilder: FormBuilder,
    private httpClient: HttpClient
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      name: ["", Validators.required],
      phone: ["", [Validators.required, Validators.pattern(/^\d{10}$/)]],
      address: ["Vue", Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    // display form values on success
    Swal.fire("ลงทะเบียนสำเร็จ!", "คุณได้ทำการลงทะเบียนเรียนแล้ว!", "success");
    // alert(
    //   "SUCCESS!! :-)\n\n" + JSON.stringify(this.registerForm.value, null, 4)
    // );

    let url =
      "https://script.google.com/macros/s/AKfycbzFx4rDH-py3F-vmPQErbllXTN4wWQZq73tZ2IcrDMlI2bR4NE/exec?path=/register";
    let json = {
      email: this.registerForm.value.email,
      fullName: this.registerForm.value.name,
      telephoneNumber: this.registerForm.value.phone,
      course: this.registerForm.value.address
    };

    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded"
      })
    };

    this.httpClient
      .post(url, json, httpOptions)
      .toPromise()
      .then(data => {
        console.log("response = " + data);
      });

    this.httpClient
      .get(
        "https://script.google.com/macros/s/AKfycbzFx4rDH-py3F-vmPQErbllXTN4wWQZq73tZ2IcrDMlI2bR4NE/exec?path=/register"
      )
      .subscribe(response => {
        this.registerForm = response["registerForm"];
      });
  }
}
